#Author: James Osborne
#Course: EGCP-4310
#Assignment: PA1
#Purpose: Basic http web server to serve files from local directory.

from socket import *

def gethostipaddress():
    return gethostbyname(gethostname())

server = socket(AF_INET, SOCK_STREAM)
#Prepare a server socket
hostIP = gethostipaddress()
print('Host IP:', hostIP)
server.bind((hostIP, 5000))
#Set server socket to listen to 1 non-primary connection at a time
server.listen(1)

while True:
    print('Ready to serve...')

    #Establish the connection
    connection, address = server.accept()
    print(address)

    try:
        #Receive response 
        message = connection.recv(1024).decode()

        #Pull filename from http request and open the file
        filename = message.split()[1]
        f = open(filename[1:])
        #Read full string of requested file
        outputString = f.read()
        outputData = []

        #Encode each character of the file string into a byte
        for character in outputString:
            outputData.append(character.encode())

        #Send one HTTP header line into socket
        connection.send(b'HTTP/1.0 200 OK\r\n\r\n')

        #Send the content of the requested file to the client
        for i in range(0, len(outputData)):
            connection.send(outputData[i])

        connection.close()
    except IOError:
        #Send response message for file not found
        connection.send(b'HTTP/1.0 404 not found\r\n\r\n')
            
        #Close connection socket
        connection.close()
    except:
        print('Something went wrong')
        break

serverSocket.close()
